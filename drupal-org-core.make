api = 2
core = 7.x

; MAKE file for Drupal core.  Used by the Drupal.org packager.

; Drupal Core
projects[drupal][type] = core
projects[drupal][download][type] = git
projects[drupal][download][url] = git://github.com/pantheon-systems/drops-7.git
projects[drupal][version] = 7.56

; *********** PATCHES ************

; Patch to prevent: user_role_grant_permissions() throws PDOException when used with non-existent permissions (e.g. a permission for a disabled/uninstalled module)
projects[drupal][patch][737816] = https://www.drupal.org/files/drupal-fix_pdoexception_grant_permissions-737816-36-do-not-test.patch
; Bug with image styles on database update
projects[drupal][patch][1973278] = http://www.drupal.org/files/issues/image-accommodate_missing_definition-1973278-16.patch
