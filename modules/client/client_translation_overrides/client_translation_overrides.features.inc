<?php
/**
 * @file
 * client_translation_overrides.features.inc
 */

/**
 * Implements hook_field_default_field_bases_alter().
 */
function client_translation_overrides_field_default_field_bases_alter(&$data) {
  if (isset($data['body'])) {
    $data['body']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['body']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_basic_file_text'])) {
    $data['field_basic_file_text']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_basic_file_text']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_basic_image_caption'])) {
    $data['field_basic_image_caption']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_basic_image_caption']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_basic_spotlight_items'])) {
    $data['field_basic_spotlight_items']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_basic_spotlight_items']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_basic_text_text'])) {
    $data['field_basic_text_text']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_basic_text_text']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_custom_link_title'])) {
    $data['field_custom_link_title']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_custom_link_title']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_custom_text'])) {
    $data['field_custom_text']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_custom_text']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_custom_title'])) {
    $data['field_custom_title']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_custom_title']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_file_author'])) {
    $data['field_file_author']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_file_author']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_file_copyright'])) {
    $data['field_file_copyright']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_file_copyright']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_file_description'])) {
    $data['field_file_description']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_file_description']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_file_image_alt_text'])) {
    $data['field_file_image_alt_text']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_file_image_alt_text']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_file_image_title_text'])) {
    $data['field_file_image_title_text']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_file_image_title_text']['translatable'] = 1; /* WAS: 0 */
  }
  if (isset($data['field_map_information'])) {
    $data['field_map_information']['settings']['entity_translation_sync'] = FALSE; /* WAS: '' */
    $data['field_map_information']['translatable'] = 1; /* WAS: 0 */
  }
}

/**
 * Implements hook_menu_default_menu_custom_alter().
 */
function client_translation_overrides_menu_default_menu_custom_alter(&$data) {
  if (isset($data['menu-secondary-menu'])) {
    $data['menu-secondary-menu']['i18n_mode'] = 5; /* WAS: '' */
    $data['menu-secondary-menu']['language'] = 'und'; /* WAS: '' */
  }
  if (isset($data['menu-tertiary-menu'])) {
    $data['menu-tertiary-menu']['i18n_mode'] = 5; /* WAS: '' */
    $data['menu-tertiary-menu']['language'] = 'und'; /* WAS: '' */
  }
}

/**
 * Implements hook_default_page_manager_pages_alter().
 */
function client_translation_overrides_default_page_manager_pages_alter(&$data) {
  if (isset($data['openethical_image_library'])) {
    $data['openethical_image_library']->default_handlers['page_openethical_image_library_panel_context']->conf['display']->panel_settings['style_settings']['footer'] = NULL; /* WAS: '' */
  }
  if (isset($data['openethical_video_library'])) {
    $data['openethical_video_library']->default_handlers['page_openethical_video_library_panel_context']->conf['display']->panel_settings['style_settings']['footer'] = NULL; /* WAS: '' */
  }
}

/**
 * Implements hook_panelizer_defaults_override_alter().
 */
function client_translation_overrides_panelizer_defaults_override_alter(&$data) {
  if (isset($data['node:oe_blog_post:default'])) {
    $data['node:oe_blog_post:default']->display->title = '%node:title_field'; /* WAS: '%node:title' */
  }
  if (isset($data['node:oe_event:default'])) {
    $data['node:oe_event:default']->display->title = '%node:title_field'; /* WAS: '%node:title' */
  }
  if (isset($data['node:oe_main_page:default'])) {
    $data['node:oe_main_page:default']->display->title = '%node:title_field'; /* WAS: '%node:title' */
  }
  if (isset($data['node:oe_project:default'])) {
    $data['node:oe_project:default']->display->title = '%node:title_field'; /* WAS: '%node:title' */
  }
  if (isset($data['node:panopoly_page:default'])) {
    $data['node:panopoly_page:default']->display->title = '%node:title_field'; /* WAS: '%node:title' */
  }
}

/**
 * Implements hook_default_search_api_index_alter().
 */
function client_translation_overrides_default_search_api_index_alter(&$data) {
  if (isset($data['openethical_blog'])) {
    $data['openethical_blog']->enabled = 0; /* WAS: 1 */
  }
  if (isset($data['openethical_document_library'])) {
    $data['openethical_document_library']->enabled = 0; /* WAS: 1 */
  }
  if (isset($data['openethical_events'])) {
    $data['openethical_events']->enabled = 0; /* WAS: 1 */
  }
  if (isset($data['openethical_image_library'])) {
    $data['openethical_image_library']->options['fields']['field_oe_file_language'] = array(
      'type' => 'string',
    ); /* WAS: '' */
  }
  if (isset($data['openethical_projects'])) {
    $data['openethical_projects']->enabled = 0; /* WAS: 1 */
  }
  if (isset($data['openethical_video_library'])) {
    $data['openethical_video_library']->options['fields']['field_oe_file_language'] = array(
      'type' => 'string',
    ); /* WAS: '' */
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function client_translation_overrides_views_default_views_alter(&$data) {
  if (isset($data['openethical_image_gallery'])) {
    $data['openethical_image_gallery']->display['default']->display_options['filters']['field_oe_file_language_value'] = array(
      'id' => 'field_oe_file_language_value',
      'table' => 'field_data_field_oe_file_language',
      'field' => 'field_oe_file_language_value',
      'value' => array(
        'und' => 'und',
      ),
      'group' => 1,
      'expose' => array(
        'operator_id' => 'field_oe_file_language_value_op',
        'label' => 'Image language',
        'operator' => 'field_oe_file_language_value_op',
        'identifier' => 'field_oe_file_language_value',
        'multiple' => TRUE,
        'remember_roles' => array(
          2 => 2,
          1 => 0,
          3 => 0,
          4 => 0,
        ),
      ),
    ); /* WAS: '' */
    $data['openethical_image_gallery']->display['default']->display_options['filters']['field_oe_file_language_value_1'] = array(
      'id' => 'field_oe_file_language_value_1',
      'table' => 'field_data_field_oe_file_language',
      'field' => 'field_oe_file_language_value',
      'operator' => 'not empty',
      'group' => 2,
    ); /* WAS: '' */
  }
  if (isset($data['openethical_video_gallery'])) {
    $data['openethical_video_gallery']->display['default']->display_options['filters']['field_oe_file_language_value'] = array(
      'id' => 'field_oe_file_language_value',
      'table' => 'field_data_field_oe_file_language',
      'field' => 'field_oe_file_language_value',
      'value' => array(
        'und' => 'und',
      ),
      'group' => 1,
      'expose' => array(
        'operator_id' => 'field_oe_file_language_value_op',
        'label' => 'Video language',
        'operator' => 'field_oe_file_language_value_op',
        'identifier' => 'field_oe_file_language_value',
        'multiple' => TRUE,
        'remember_roles' => array(
          2 => 2,
          1 => 0,
          3 => 0,
          4 => 0,
        ),
      ),
    ); /* WAS: '' */
    $data['openethical_video_gallery']->display['default']->display_options['filters']['field_oe_file_language_value_1'] = array(
      'id' => 'field_oe_file_language_value_1',
      'table' => 'field_data_field_oe_file_language',
      'field' => 'field_oe_file_language_value',
      'operator' => 'not empty',
      'group' => 2,
    ); /* WAS: '' */
  }
}
