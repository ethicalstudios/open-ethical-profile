api = 2
core = 7.x

; MAKE file for Open Ethical.

; ************************************************
; ******************* OPEN ETHICAL *******************

projects[ethical_admin][type] = "module"
projects[ethical_admin][download][type] = git
projects[ethical_admin][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_admin.git"
projects[ethical_admin][download][tag] = "v7.x-1.92"
projects[ethical_admin][subdir] = openethical

projects[ethical_audio][type] = "module"
projects[ethical_audio][download][type] = git
projects[ethical_audio][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_audio.git"
projects[ethical_audio][download][tag] = "v7.x-1.8"
projects[ethical_audio][subdir] = openethical

projects[ethical_blog][type] = "module"
projects[ethical_blog][download][type] = git
projects[ethical_blog][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_blog.git"
projects[ethical_blog][download][tag] = "v7.x-1.8"
projects[ethical_blog][subdir] = openethical

projects[ethical_document_library][type] = "module"
projects[ethical_document_library][download][type] = git
projects[ethical_document_library][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_document_library.git"
projects[ethical_document_library][download][tag] = "v7.x-1.8"
projects[ethical_document_library][subdir] = openethical

projects[ethical_documents][type] = "module"
projects[ethical_documents][download][type] = git
projects[ethical_documents][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_documents.git"
projects[ethical_documents][download][tag] = "v7.x-1.8"
projects[ethical_documents][subdir] = openethical

projects[ethical_events][type] = "module"
projects[ethical_events][download][type] = git
projects[ethical_events][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_events.git"
projects[ethical_events][download][tag] = "v7.x-1.8"
projects[ethical_events][subdir] = openethical

projects[ethical_faceted][type] = "module"
projects[ethical_faceted][download][type] = git
projects[ethical_faceted][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_faceted.git"
projects[ethical_faceted][download][tag] = "v7.x-1.8"
projects[ethical_faceted][subdir] = openethical

projects[ethical_image_library][type] = "module"
projects[ethical_image_library][download][type] = git
projects[ethical_image_library][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_image_library.git"
projects[ethical_image_library][download][tag] = "v7.x-1.8"
projects[ethical_image_library][subdir] = openethical

projects[ethical_images][type] = "module"
projects[ethical_images][download][type] = git
projects[ethical_images][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_images.git"
projects[ethical_images][download][tag] = "v7.x-1.8"
projects[ethical_images][subdir] = openethical

projects[ethical_media][type] = "module"
projects[ethical_media][download][type] = git
projects[ethical_media][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_media.git"
projects[ethical_media][download][tag] = "v7.x-1.8"
projects[ethical_media][subdir] = openethical

projects[ethical_pages][type] = "module"
projects[ethical_pages][download][type] = git
projects[ethical_pages][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_pages.git"
projects[ethical_pages][download][tag] = "v7.x-1.8"
projects[ethical_pages][subdir] = openethical

projects[ethical_partners][type] = "module"
projects[ethical_partners][download][type] = git
projects[ethical_partners][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_partners.git"
projects[ethical_partners][download][tag] = "v7.x-1.8"
projects[ethical_partners][subdir] = openethical

projects[ethical_projects][type] = "module"
projects[ethical_projects][download][type] = git
projects[ethical_projects][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_projects.git"
projects[ethical_projects][download][tag] = "v7.x-1.8"
projects[ethical_projects][subdir] = openethical

projects[ethical_social][type] = "module"
projects[ethical_social][download][type] = git
projects[ethical_social][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_social.git"
projects[ethical_social][download][tag] = "v7.x-1.8"
projects[ethical_social][subdir] = openethical

projects[ethical_stories][type] = "module"
projects[ethical_stories][download][type] = git
projects[ethical_stories][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_stories.git"
projects[ethical_stories][download][tag] = "v7.x-1.8"
projects[ethical_stories][subdir] = openethical

projects[ethical_taxonomy][type] = "module"
projects[ethical_taxonomy][download][type] = git
projects[ethical_taxonomy][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_taxonomy.git"
projects[ethical_taxonomy][download][tag] = "v7.x-1.8"
projects[ethical_taxonomy][subdir] = openethical

projects[ethical_theme][type] = "module"
projects[ethical_theme][download][type] = git
projects[ethical_theme][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_theme.git"
projects[ethical_theme][download][tag] = "v7.x-1.9"
projects[ethical_theme][subdir] = openethical

projects[ethical_translation][type] = "module"
projects[ethical_translation][download][type] = git
projects[ethical_translation][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_translation.git"
projects[ethical_translation][download][tag] = "v7.x-1.9"
projects[ethical_translation][subdir] = openethical

projects[ethical_video_library][type] = "module"
projects[ethical_video_library][download][type] = git
projects[ethical_video_library][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_video_library.git"
projects[ethical_video_library][download][tag] = "v7.x-1.8"
projects[ethical_video_library][subdir] = openethical

projects[ethical_videos][type] = "module"
projects[ethical_videos][download][type] = git
projects[ethical_videos][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_videos.git"
projects[ethical_videos][download][tag] = "v7.x-1.8"
projects[ethical_videos][subdir] = openethical

projects[ethical_widgets][type] = "module"
projects[ethical_widgets][download][type] = git
projects[ethical_widgets][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/ethical_widgets.git"
projects[ethical_widgets][download][tag] = "v7.x-1.8"
projects[ethical_widgets][subdir] = openethical

projects[radix][version] = 7.x-3.0+3-dev
projects[radix][type] = theme
projects[radix][download][type] = git

projects[radix_frisbee][type] = "theme"
projects[radix_frisbee][download][type] = git
projects[radix_frisbee][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/radix_frisbee.git"
projects[radix_frisbee][download][tag] = "v7.x-1.9"



; ************************************************
; ******************* PANOPOLY *******************

; Note that makefiles are parsed bottom-up, and that in Drush 5 concurrency might
; interfere with recursion.
; Therefore PANOPOLY needs to be listed AT THE BOTTOM of this makefile,
; so we can patch or update certain projects fetched by Panopoly's makefiles.
; NOTE: If you are running Drush 6, this section should be places at the TOP
; WE ARE RUNNING DRUSH 6

; Someday maybe we can turn this on to just inherit Panopoly
;projects[panopoly][type] = profile
;projects[panopoly][version] = 1.47
; but, Drupal.org does not support recursive profiles
; and also does not support include[]
; so we need to copy the panopoly.make file here

; The Panopoly Foundation

projects[panopoly_core][version] = 1.47
projects[panopoly_core][subdir] = panopoly

projects[panopoly_theme][version] = 1.47
projects[panopoly_theme][subdir] = panopoly

projects[panopoly_magic][version] = 1.47
projects[panopoly_magic][subdir] = panopoly
projects[panopoly_magic][patch][] = patches/panopoly_magic_show_view_modes_by_default.patch

projects[panopoly_widgets][version] = 1.47
projects[panopoly_widgets][subdir] = panopoly
projects[panopoly_widgets][patch][] = patches/panopoly_widgets_reset_page_content_category.patch

projects[panopoly_admin][version] = 1.47
projects[panopoly_admin][subdir] = panopoly

projects[panopoly_users][version] = 1.47
projects[panopoly_users][subdir] = panopoly

; The Panopoly Toolset

projects[panopoly_wysiwyg][version] = 1.47
projects[panopoly_wysiwyg][subdir] = panopoly
projects[panopoly_wysiwyg][patch][] = patches/panopol_wysiwyg_add_spam_span.patch

projects[panopoly_search][version] = 1.47
projects[panopoly_search][subdir] = panopoly


; ***************** End Panopoly *****************
; ************************************************


; ************************************************
; ************** PANOPOLY OVERRIDES **************

; From Panopoly Core

projects[ctools][version] = 1.12
projects[ctools][subdir] = contrib
projects[ctools][patch][2607626] = https://www.drupal.org/files/issues/ctools-close-modal-2607626-5.patch
projects[ctools][patch][2787045] = https://www.drupal.org/files/issues/ctools-jquery_compatibility-2787045-14.patch
; additional patches for OE
projects[ctools][patch][2433923] = https://www.drupal.org/files/issues/ctools-fix_pager_issue-2433923-23.patch


; Need to stick with 3.14 as anything higher breaks the 'How far have we come' total at the top of the Bonn Challenge homepage.
projects[views][version] = 3.14
projects[views][subdir] = contrib
projects[views][patch][2037469] = http://drupal.org/files/views-exposed-sorts-2037469-1.patch
; additional patches for OE
projects[views][patch][1959558] = https://www.drupal.org/files/fixed_image_warnings-1959558-1.patch

projects[uuid][version] = 1.0-alpha6
projects[uuid][subdir] = contrib
; additional patches for OE
projects[uuid][patch][2186687] = https://www.drupal.org/files/issues/uuid_git.patch

projects[fieldable_panels_panes][version] = 1.11
projects[fieldable_panels_panes][subdir] = contrib
; additional patches for OE
projects[fieldable_panels_panes][patch][] = patches/fpp-remove-stuff-for-editors.patch

;From Panopoly Admin

; Navbar from dev to 1.5
projects[navbar][version] = 1.5
projects[navbar][subdir] = contrib


;From Panopoly Theme

; Override panopoly_theme.make: including footers in patch
projects[radix_layouts][version] = 3.4
projects[radix_layouts][subdir] = contrib
; additional patches for OE
projects[radix_layouts][patch][2576995] = https://www.drupal.org/files/issues/footers_for_all-2576995-2.patch


;From Panopoly Search

projects[search_api_solr][version] = 1.8
projects[search_api_solr][subdir] = contrib
; additional patches for OE
projects[search_api_solr][patch][] = patches/search_api_solr-connect-to-external-solr.patch

;From Panopoly widgets
projects[media_vimeo][version] = 2.1
projects[media_vimeo][subdir] = contrib
projects[media_vimeo][patch][2446199] = https://www.drupal.org/files/issues/no_exception_handling-2446199-1.patch
; additional patches for OE
projects[media_vimeo][patch][] = patches/allow-thumbnail-404.patch

;Dev of media_youtube to fix https://www.drupal.org/node/2498493
projects[media_youtube][version] = 3.x-dev
projects[media_youtube][subdir] = contrib
projects[media_youtube][download][branch] = 7.x-3.x
projects[media_youtube][download][revision] = 40dc4feb0230fd4268cf09b0d08adbec55b19b67

; ************************************************
; ******************* OPEN ETHICAL contrib *******************

; Countries
projects[countries][version] = 2.x-dev
projects[countries][subdir] = contrib
projects[countries][download][type] = git
projects[countries][download][branch] = 7.x-2.x
projects[countries][download][revision] = 21eae4f
projects[countries][patch][2407063] = https://www.drupal.org/files/issues/countries-permissions-2407063-4.patch
projects[countries][patch][1660370] = https://www.drupal.org/files/issues/countries-entity-translation-1660370-29.patch

; Disqus
projects[disqus][version] = 1.10
projects[disqus][subdir] = contrib

; Easy social
projects[easy_social][version] = 2.12
projects[easy_social][subdir] = contrib
projects[easy_social][patch][2342875] = https://www.drupal.org/files/issues/easy_social-footer_scope_2342875_4.patch

; Entity cache
projects[entitycache][version] = 1.2
projects[entitycache][subdir] = contrib

; EU cookie compliance
projects[eu_cookie_compliance][version] = 1.14
projects[eu_cookie_compliance][subdir] = contrib

; Facetapi multiselect
projects[facetapi_multiselect][version] = 1.x-dev
projects[facetapi_multiselect][subdir] = contrib
projects[facetapi_multiselect][download][type] = git
projects[facetapi_multiselect][download][branch] = 7.x-1.x
projects[facetapi_multiselect][download][revision] = 0ba5a1a
projects[facetapi_multiselect][patch][1806344] = https://www.drupal.org/files/issues/facetapi_multiselect-new_features-1806344-26.patch
projects[facetapi_multiselect][patch][1910720] = https://www.drupal.org/files/rawurlencode_solr_key-1910720-4.patch
projects[facetapi_multiselect][patch][2150749] = https://www.drupal.org/files/issues/broken_pagination-2150749-2.patch

; Fences
projects[fences][version] = 1.x-dev
projects[fences][subdir] = contrib
projects[fences][download][type] = git
projects[fences][download][branch] = 7.x-1.x
projects[fences][download][revision] = 67206b5

; @font-your-face
projects[fontyourface][version] = 2.8
projects[fontyourface][subdir] = contrib

; Global redirect
projects[globalredirect][version] = 1.5
projects[globalredirect][subdir] = contrib

; Google analytics
projects[google_analytics][version] = 2.3
projects[google_analytics][subdir] = contrib

; Manual Crop
; Copied from Panopoly Images (not used by Open Ethical)

projects[manualcrop][version] = 1.x-dev
projects[manualcrop][subdir] = contrib
projects[manualcrop][download][type] = git
projects[manualcrop][download][revision] = d6c449d
projects[manualcrop][download][branch] = 7.x-1.x

; Manualcrop has its own .make file which gets these.

; Libraries for Manual crop

; imagesLoaded.
libraries[jquery.imagesloaded][download][type] = file
libraries[jquery.imagesloaded][download][url] = https://github.com/desandro/imagesloaded/archive/v2.1.2.tar.gz
libraries[jquery.imagesloaded][download][subtree] = imagesloaded-2.1.2

; imgAreaSelect.
libraries[jquery.imgareaselect][download][type] = file
libraries[jquery.imgareaselect][download][url] = https://github.com/odyniec/imgareaselect/archive/v0.9.11-rc.1.tar.gz
libraries[jquery.imgareaselect][download][subtree] = imgareaselect-0.9.11-rc.1

; Media soundcloud
projects[media_soundcloud][version] = 2.1
projects[media_soundcloud][subdir] = contrib

; Menu position
projects[menu_position][version] = 1.2
projects[menu_position][subdir] = contrib

; Multiple forms
projects[multiform][version] = 1.3
projects[multiform][subdir] = contrib
projects[multiform][patch][] = patches/multiform-remove_image_upload_btns-no_issue.patch

; Oauth
projects[oauth][version] = 3.4
projects[oauth][subdir] = contrib

; Panopoly SEO
projects[panopoly_seo][version] = 1.0-beta10
projects[panopoly_seo][subdir] = contrib

; Plupload
projects[plupload][version] = 1.7
projects[plupload][subdir] = contrib

; Smart trim
projects[smart_trim][version] = 1.5
projects[smart_trim][subdir] = contrib
projects[smart_trim][patch][2490690] = https://www.drupal.org/files/issues/smart-trim-php-notices-fix.patch

; Spam span
projects[spamspan][version] = 1.x-dev
projects[spamspan][subdir] = contrib
projects[spamspan][download][branch] = 7.x-1.x
projects[spamspan][download][revision] = cebf9dd
projects[spamspan][patch][] = patches/spamspan-moveadmin.patch

; Taxonomy access fix
projects[taxonomy_access_fix][version] = 2.3
projects[taxonomy_access_fix][subdir] = contrib

; Twitter
projects[twitter][version] = 5.11
projects[twitter][subdir] = contrib

; Twitter pull
projects[twitter_pull][version] = 2.0-alpha3
projects[twitter_pull][subdir] = contrib

; Variable
projects[variable][version] = 2.5
projects[variable][subdir] = contrib

; **** Themes ***************************************

; Radix Theme
projects[radix][version] = 3.x-dev
projects[radix][download][branch] = 7.x-3.x
projects[radix][download][revision] = cb2e6b5



; ************************************************
; ******************* OPEN ETHICAL ml contrib *******************

; Entity translation
projects[entity_translation][version] = 1.0-beta6
projects[entity_translation][subdir] = contrib_ml

; Facet api translation
projects[facetapi_i18n][version] = 1.0-beta2
projects[facetapi_i18n][subdir] = contrib_ml

; i18n
projects[i18n][version] = 1.x-dev
projects[i18n][subdir] = contrib_ml
projects[i18n][download][type] = git
projects[i18n][download][branch] = 7.x-1.x
projects[i18n][download][revision] = c5ed2b2

; Lang dropdown
projects[lang_dropdown][version] = 2.5
projects[lang_dropdown][subdir] = contrib_ml

; List predefined options
projects[list_predefined_options][version] = 1.0
projects[list_predefined_options][subdir] = contrib_ml

; Search api entity translate
projects[search_api_et][version] = 2.x-dev
projects[search_api_et][subdir] = contrib_ml
projects[search_api_et][download][type] = git
projects[search_api_et][download][branch] = 7.x-2.x
projects[search_api_et][download][revision] = e8994c9

; Search api entity translate db
projects[search_api_et_db][version] = 1.x-dev
projects[search_api_et_db][subdir] = contrib_ml
projects[search_api_et_db][download][type] = git
projects[search_api_et_db][download][branch] = 7.x-1.x
projects[search_api_et_db][download][revision] = 1cc1a8b

; Search api entity translate solr
projects[search_api_et_solr][version] = 1.x-dev
projects[search_api_et_solr][subdir] = contrib_ml
projects[search_api_et_solr][download][type] = git
projects[search_api_et_solr][download][branch] = 7.x-1.x
projects[search_api_et_solr][download][revision] = a03edb6

; Title
projects[title][version] = 1.x-dev
projects[title][subdir] = contrib_ml
projects[title][download][type] = git
projects[title][download][branch] = 7.x-1.x
projects[title][download][revision] = d6f2000
